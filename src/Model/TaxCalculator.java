package Model;

import java.util.ArrayList;

import Interface.Taxable;

public class TaxCalculator {
	public static double result(ArrayList<Taxable> taxList){
		double sum = 0;
		for(Taxable taxObj:taxList)
			sum += taxObj.getTaxable();
		
		return sum;
	}
}
