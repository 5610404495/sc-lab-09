package Model;
import java.util.Comparator;

import Interface.Taxable;

public class TaxComparator implements Comparator {
	@Override
		public int compare(Object o1, Object o2){		
		Taxable t1 = (Taxable)o1;		
		Taxable t2 = (Taxable)o2;
		
		if (t1.getTaxable() > t2.getTaxable()) 
			return 1;
		else if (t1.getTaxable() < t2.getTaxable()) 
			return -1;
		else
		return 0;		
	}
	

}
