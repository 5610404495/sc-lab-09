package Model;

import Interface.Measurable;

public class Country implements Measurable {
	private String name;
	private int man;

	public Country(String name, int man) {
		this.name = name;
		this.man = man;
	}

	public String getName() {
		return this.name;
	}

	public int getMan() {
		return this.man;
	}

	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return this.getMan();
	}

}
