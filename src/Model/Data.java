package Model;

import Interface.Measurable;

public class Data {
		
	public static Measurable min(Measurable m1, Measurable m2) {
		if (m1.getMeasure() < m2.getMeasure()) 
			return m1;
		 else 
			return m2;
	}
	
	public static double avg(Measurable[] measure) {
		double sum = 0;
		for (Measurable m : measure) 
			sum += m.getMeasure();
		
		if (measure.length > 0) 
			return sum / measure.length;
		else 
			return 0;
		
	}
}
