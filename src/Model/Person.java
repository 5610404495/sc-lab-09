package Model;

import Interface.Measurable;
import Interface.Taxable;

public class Person implements Measurable, Taxable , Comparable {

	private String name;
	private double height;
	private double yearlyReceipts;


	public Person(String name, double height) {
		this.name = name;
		this.height = height;
	}

	public Person(String name, double height, double yearlyReceipts) {
		this(name, height);
		this.yearlyReceipts = yearlyReceipts;
	}

	public String getName() {
		return this.name;
	}

	public double getHeight() {
		return this.height;
	}

	public double getYearlyReceipts() {
		return this.yearlyReceipts;
	}

	@Override
	public double getMeasure() {
		return this.getHeight();
	}

	@Override
	public double getTaxable() {
		double income = this.getYearlyReceipts();
		double sum = 0;

		if (income > 300000) {
			sum += 0.05 * 300000;
			income -= 300000;
			sum += 0.10 * income;
		}else{
			sum += 0.05 * income;
		}
		return sum;
	}

	@Override
	public int compareTo(Object obj) {
		  if (this.yearlyReceipts == ((Person) obj).getYearlyReceipts())
	            return 0;
	        else if ((this.yearlyReceipts) > ((Person) obj).getYearlyReceipts())
	            return 1;
	        else
	        	return -1;
	}
	public String toString() {
        return "Name " + name + " Receipts " + yearlyReceipts;
    }
}
