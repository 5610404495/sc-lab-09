package Model;

import Interface.Taxable;

public class Product implements Taxable, Comparable {
	private String name;
	private double value;

	public Product(String name, double value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return this.name;
	}

	public double getValue() {
		return this.value;
	}

	@Override
	public double getTaxable() {
		return 0.07 * this.getValue();
	}

	@Override
	public int compareTo(Object obj) {
		if (this.value == ((Product) obj).getValue())
			return 0;
		else if ((this.value) > ((Product) obj).getValue())
			return 1;
		else 
			return -1;
	}
	public String toString() {
        return  name + " Costs " + value;
    }
}
