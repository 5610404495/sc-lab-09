package Model;

import Interface.Taxable;

public class Company implements Taxable {
	private String name;
	private double recep;
	private double exp;

	public Company(String name, double receipts, double expenses) {
		this.name = name;
		this.recep = receipts;
		this.exp = expenses;
	}

	public String getName() {
		return this.name;
	}

	public double getRecep() {
		return this.recep;
	}

	public double getExp() {
		return this.exp;
	}
	public double getProfit(){
		return recep-exp;
		
	}

	@Override
	public double getTaxable() {
		return 0.3 * (this.getRecep() - this.getExp());
	}
	public String toString() {
        return  name + " Receipts " + recep + " Expenses "+ exp + " Profit " +(recep-exp);
    }
}
