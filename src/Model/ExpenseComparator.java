package Model;

import java.util.Comparator;

public class ExpenseComparator implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		Company c1=(Company)o1;  
		Company c2=(Company)o2;  
		  
		if (c1.getExp() == c2.getExp())
			return 0;
        else if (c1.getExp() > c2.getExp())
            return 1;
        else
        	return -1;
        
	}
 
}
