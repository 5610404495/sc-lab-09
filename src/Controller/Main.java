package Controller;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import Interface.Measurable;
import Interface.Taxable;
import Model.Company;
import Model.Country;
import Model.Data;
import Model.EarningComparator;
import Model.ExpenseComparator;
import Model.Person;
import Model.Product;
import Model.ProfitComparator;
import Model.TaxCalculator;
import Model.TaxComparator;

public class Main {

	public static void main(String[] args) {
		Main main = new Main();
		main.Product();
		main.Company();
		main.Person();
		main.Tax();
	}
	
public void Product() {
		ArrayList<Product> list = new ArrayList<Product>();
		list.add(new Product("CaradaNakket", 20));
		list.add(new Product("Iphone399", 30000));
		list.add(new Product("Subaru", 120000));
		Collections.sort(list);

		Iterator it = list.iterator();

		while (it.hasNext()) {
			Object e = it.next();
			System.out.println(e + "\n");
		}
		System.out.println("------------------------------------------"
				+ "\n");
	}

	public void Company() {
		ArrayList<Company> list = new ArrayList<Company>();
		list.add(new Company("Redbull", 200000, 100000));
		list.add(new Company("Bamhee Nong", 20000, 3000));
		list.add(new Company("GMM", 1900000, 700000));

		Collections.sort(list, new EarningComparator());
		Iterator it = list.iterator();
		System.out.println("-------------Sort By Receipt--------------");
		while (it.hasNext()) {
			Object e = it.next();
			System.out.println(e + "\n");

		}
		Collections.sort(list, new ExpenseComparator());
		Iterator it2 = list.iterator();
		System.out.println("-------------Sort By Expense---------------");
		while (it2.hasNext()) {
			Object e = it2.next();
			System.out.println(e + "\n");
		}
		
		Collections.sort(list, new ProfitComparator());
		Iterator it3 = list.iterator();
		System.out.println("--------------Sort By Profit-----------------");
		while (it3.hasNext()) {
			Object e = it3.next();
			System.out.println(e + "\n");
		}
		System.out.println("------------------------------------------"
				+ "\n");
	}
	public void Person() {

		ArrayList<Person> list = new ArrayList<Person>();
		list.add(new Person("Earl", 167, 3000000));
		list.add(new Person("Tom", 190, 20));
		list.add(new Person("Elena", 155, 30200));
		Collections.sort(list);

		Iterator it = list.iterator();

		while (it.hasNext()) {
			Object e = it.next();
			System.out.println(e + "\n");

		}
		System.out.println("------------------------------------------");
	}
	
private void Tax() {
		ArrayList<Object> list = new ArrayList <Object>();
		list.add(new Person("Robert", 195, 178000));
		list.add(new Product("Chopper", 520000));
		list.add(new Company("Kantana Group", 2001400, 300000));
		
		Collections.sort(list, new TaxComparator());
		Iterator it = list.iterator();

		while (it.hasNext()) {
			Object e = it.next();
			System.out.println(e + "\n");
		}
		
	}

	
}
