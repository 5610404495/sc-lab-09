package ModelTree;



public class TraverseTest {

	public static void main(String[] args) {
		Node C = new Node("C",null,null);
		Node E = new Node("E",null,null);
		Node D = new Node("D",C,E);
		Node A = new Node("A",null,null);
		Node B = new Node("B",A,D);
		Node H = new Node("H",null,null);
		Node I = new Node("I",H,null);
		Node G = new Node("G",null,I);
		Node F = new Node("F",B,G);
		
		ReportConsole console = new ReportConsole();
		PreOrderTraversal pre = new PreOrderTraversal();
		PostOrderTraversal post = new PostOrderTraversal();
		InOrderTraversal in = new InOrderTraversal();
		
		System.out.print("Traverse with ProOrderTraversal : ");
		console.display(F, pre);
		System.out.print("Traverse with InOrderTraversal : ");
		console.display(F, in);
		System.out.print("Traverse with PostOrderTraversal : ");
		console.display(F, post);
	}

}
