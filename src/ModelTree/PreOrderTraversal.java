package ModelTree;



import java.util.ArrayList;
import java.util.List;



public class PreOrderTraversal implements Traversal{
public List<Node> traverse(Node node) {
		
		List<Node>  listNode = new ArrayList<Node>()  ; 
		listNode.add(node);
		
		if(node.getLeft() != null){
			List<Node> l = traverse(node.getLeft());
			
			for (Node e  : l){
				listNode.add(e);
			}
		}

		if (node.getRight() != null){
			List<Node> r = traverse(node.getRight());
			
			for (Node e  : r){
				listNode.add(e);
			}
		}	
		
		return listNode;		
	}
}
