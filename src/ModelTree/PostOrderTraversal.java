package ModelTree;

import java.util.ArrayList;
import java.util.List;

public class PostOrderTraversal implements Traversal{
	public List<Node> traverse(Node node){
		List<Node>  listNode = new ArrayList<Node>() ; 
		
		
		if(node.getLeft() != null){
			List<Node> l = traverse(node.getLeft());
			
			for (Node e  : l){
				listNode.add(e);
			}
		}

		if (node.getRight() != null){
			List<Node> r = traverse(node.getRight());
			
			for (Node e  : r){
				listNode.add(e);
			}
		}	
		listNode.add(node);
		return listNode;		
	}
}
