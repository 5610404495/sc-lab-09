package ModelTree;

import java.util.List;



public class ReportConsole {
	public void display(Node root, Traversal traversal){
		List<Node> showTree = traversal.traverse(root);
		for (Node x : showTree){
			System.out.print(x.getValue()+" ");
		}
		System.out.println();
	}
}
