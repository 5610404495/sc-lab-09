package ModelTree;


import java.util.ArrayList;
import java.util.List;



public class InOrderTraversal implements Traversal{
	public List<Node> traverse(Node node){
		List<Node> listNode = new ArrayList<Node>();	
		
		if(node.getLeft() != null){
			List<Node> l = traverse(node.getLeft());
			
			for (Node x : l){
				listNode.add(x);
			}
		}
		
		listNode.add(node);
		
		if (node.getRight() != null){
			List<Node> r = traverse(node.getRight());
			
			for (Node x : r){
				listNode.add(x);
			}
		}		
		return listNode;		
	}
	
}
